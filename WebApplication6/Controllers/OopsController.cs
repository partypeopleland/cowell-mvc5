﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication6.Controllers
{
    public class OopsController : Controller
    {
        [HandleError(Master = "_Layout", ExceptionType = typeof(ArgumentException), View = "Error.Arg")]
        public ActionResult Index(string type="")
        {
            if (type == "1") {
                throw new ArgumentException("Oops~發生錯誤囉~");
            } else {
                throw new Exception("OK");
            }

        }
    }
}