﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication6.ViewModel;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{
    public class MBController : Controller
    {

        public ActionResult RQ() {
            ViewBag.name = HttpContext.Request.QueryString["name"];
            return View();
        }
        public ActionResult RQ2(string name) {
            ViewBag.name = name;
            return View("RQ");
        }

        public ActionResult Simple1() {
            return View();
        }

        [HttpPost]
        public ActionResult Simple1(string username, string password) {
            return Content(String.Format("UserName:[{0}] Password:[{1}]", username, password));
        }
        public ActionResult Simple2() {
            return View("Simple1");
        }

        [HttpPost]
        public ActionResult Simple2(FormCollection frm1) {
            return Content(String.Format("UserName:[{0}] Password:[{1}]", frm1["username"], frm1["password"]));
        }

        public ActionResult Complex1() {
            return View("Simple1");
        }
        [HttpPost]
        public ActionResult Complex1(Simple1MBViewModel frm) {
            return Content(String.Format("UserName:[{0}] Password:[{1}]", frm.username, frm.password));
        }

        public ActionResult Complex2() {
            return View();
        }

        [HttpPost]
        public ActionResult Complex2(Simple1MBViewModel item1, Simple1MBViewModel item2) {
            return Content("Complex2: "
                 + item1.username + ":" + item1.password
                 + " | "
                 + item2.username + ":" + item2.password);
        }
        public ActionResult Complex3() {
            return View("Complex2");
        }
        [HttpPost]
        public ActionResult Complex3([Bind(Prefix = "item1")]Simple1MBViewModel frm) {
            return Content(String.Format("UserName:[{0}] Password:[{1}]", frm.username, frm.password));
        }

        public ActionResult Complex4() {
            NORTHWNDEntities db = new NORTHWNDEntities();
            var data = from p in db.Employees
                       select new Simple1MBViewModel() {
                           username = p.FirstName,
                           password = p.LastName,
                           age = 18
                       };
            return View(data.Take(10));
        }

        [HttpPost]
        public ActionResult Complex4(IList<Simple1MBViewModel> item) {
            //由中斷點檢查所接到的資料內容
            return Content("請由中斷點檢查所接到的資料內容");
        }
        public ActionResult Complex5() {
            return View();
        }
        [HttpPost]
        public ActionResult Complex5(FormCollection frm) {
            Simple1MBViewModel item = new Simple1MBViewModel();
            if (TryUpdateModel<Simple1MBViewModel>(item, "", frm.AllKeys, new string[] { "id" })) {
                return RedirectToAction("Complex5");
            }
            return View(item);
        }
    }
}