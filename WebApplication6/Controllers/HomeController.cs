﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Helpers;
using WebApplication6.Models;
using WebApplication6.ActionFilter;
using System.Web.Security;
using WebApplication6.ViewModel;

namespace WebApplication6.Controllers {

    //[Authorize]
    [Logger]
    public class HomeController : BaseController {
        [AllowAnonymous]
        public ActionResult Login(string ReturnUrl) {
            //ReturnUrl字串是使用者在未登入情況下要求的的Url
            LoginVM vm = new LoginVM() { ReturnUrl = ReturnUrl };
            return View(vm);
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginVM vm) {

            //沒通過Model驗證(必填欄位沒填，DB無此帳密)
            if (!ModelState.IsValid) {
                return View(vm);
            }

            //都成功...
            //進行表單登入 ※之後User.Identity.Name的值就是vm.Account帳號的值
            //導向預設Url(Web.config裡的defaultUrl定義)或使用者原先Request的Url
            FormsAuthentication.RedirectFromLoginPage(vm.Account, false);

            //剛剛已導向，此行不會執行到
            return Redirect(FormsAuthentication.GetRedirectUrl(vm.Account, false));
        }
        [AllowAnonymous]
        public ActionResult Logout() {
            //清除Session中的資料
            Session.Abandon();
            //登出表單驗證
            FormsAuthentication.SignOut();

            return RedirectToAction("index", "Home");
        }


        public ActionResult Index() {
            return View();
        }
        #region Day1 Action
        public ActionResult Caltest() {
            double s = 32.12;
            double q = 333.111;
            double w = 0;
            ViewBag.Ans = q / w;
            return View();
        }

        public ActionResult WebEssentials2013() {
            return View();
        }
        public ActionResult 中斷點() {
            return View();
        }
        public ActionResult DataTips() {
            return View();
        }
        public ActionResult 工具箱() {
            return View();
        }
        public ActionResult 視窗分割() {
            return View();
        }
        public ActionResult 工作清單() {
            return View();
        }
        public ActionResult 條件式編譯() {
            return View();
        }
        public ActionResult DBFirst() {
            return View();
        }
        public ActionResult ModelQuery() {
            return View();
        }
        public ActionResult 倉儲模式() {
            return View();
        }
        #endregion

        #region Day2 Action
        public ActionResult 擴充資料模型() {
            return View();
        }
        public ActionResult 表單驗證() {
            return View();
        }
        public ActionResult InputValidation() {
            return View();
        }
        public ActionResult ModelValidation() {
            return View();
        }
        public ActionResult Routing() {
            return View();
        }
        public ActionResult 多語系() {
            return View();
        }
        #endregion

        #region Day3 Action
        public ActionResult FileResult() {
            return View();
        }
        public ActionResult RedirectResult() {
            return View();
        }
        public ActionResult ContentResult() {
            return View();
        }
        public ActionResult JavaScriptResult() {
            return View();
        }
        public ActionResult JSONResult() {
            return View();
        }
        public ActionResult HttpStatusCodeResult() {
            return View();
        }
        public ActionResult ViewResult() {
            return View();
        }
        #endregion

        #region Day4 Action
        public ActionResult RQ() {
            return View();
        }
        public ActionResult SimpleBinding() {
            return View();
        }
        public ActionResult ComplexBinding() {
            return View();
        }
        public ActionResult TryUpdateModel() {
            return View();
        }
        public ActionResult CustomError() {
            return View();
        }
        public ActionResult LifeCycleEazy() {
            return View();
        }
        public ActionResult DataTrans() {
            return View();
        }
        public ActionResult DataTransfer() {
            //存放於TempData、ViewBag、ViewData
            TempData["MSG"] = "Message in TempData";
            ViewData["MSG"] = "Message in ViewData";
            Session["MSG"] = "Message in Session";
            return RedirectToAction("DataTransferResult");
        }
        public ActionResult DataTransferResult() {
            ViewBag.MSG1 = TempData["MSG"];
            ViewBag.MSG2 = ViewData["MSG"];
            ViewBag.MSG3 = Session["MSG"];

            ViewData["TEST"] = "ViewBag and ViewData Test";
            return View();
        }
        #endregion

        #region Day5 Action
        public ActionResult LifeCycle() {
            return View();
        }
        public ActionResult Filter() {
            return View();
        }
        public ActionResult Glimpse() {
            return View();
        }
        #endregion

        #region Day6 Action
        public ActionResult 表單驗證實作() {
            return View();
        }
        public ActionResult 主版頁面框架() {
            return View();
        }
        public ActionResult 套版練習() {
            return View();
        }
        #endregion

        public ActionResult Day7() {
            return View();
        }
        
        public ActionResult About() {
            ViewBag.Message = "Your application description page.";

            return View();
        }
            
        public ActionResult Contact() {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [AllowAnonymous]
        public ActionResult Lang(string culture) {
            culture = CultureHelper.GetImplementedCulture(culture);
            HttpCookie cookie = Request.Cookies["_culture"];
            if (cookie != null)
                cookie.Value = culture;//更新cookie語言特性
            else {
                cookie = new HttpCookie("_culture");
                cookie.HttpOnly = false;
                cookie.Value = culture;
                cookie.Expires = DateTime.Now.AddMonths(3);
            }
            Response.Cookies.Add(cookie);
            return RedirectToAction("index");
        }
        public ActionResult USP() {
            NORTHWNDEntities db = new NORTHWNDEntities();
            var Top10 = db.Ten_Most_Expensive_Products();
            return View(Top10);
        }
    }
}