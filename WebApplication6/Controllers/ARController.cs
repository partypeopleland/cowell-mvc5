﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{
    public class ARController : Controller
    {
        NORTHWNDEntities db = new NORTHWNDEntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult test() {
            var Objs = db.Customers.Take(5);
            db.Configuration.LazyLoadingEnabled = false;
            return Json(Objs,JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowPIC(string url,string DL="n") {
            if (String.IsNullOrEmpty(url)) {
                url = "http://www.triumphtaiwan.com/2011-moto/speed-triple/TOP-BANNER.jpg";
                }

            WebClient wc = new WebClient();
            byte[] content = wc.DownloadData(url);
            if (DL.ToLower() == "n")
                return File(content, "image/jpeg");
            else
                return File(content, "image/jpeg", "圖片.jpg");
        }

        public ActionResult redir302() {
            //HTTP 302 example
            return RedirectToAction("ShowPIC", "AR", new { url = "http://www.imageshop.com.tw/pic/shop/home/men-world.jpg" });
        }
        public ActionResult redir301() {
            //HTTP 301 example
            return RedirectToActionPermanent("ShowPIC", "AR", new { url = "http://www.imageshop.com.tw/pic/shop/home/men-world.jpg" });
        }

        public ActionResult JsonResult() {
            
            
            //防止因循環參考造成的序列化錯誤
            //db.Configuration.LazyLoadingEnabled = false;    
            var customers = db.Customers;

            return Json(customers, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Page404() {
            return HttpNotFound();
        }
        public ActionResult Page400() {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
        [HttpGet]
        public ActionResult Upload() {
            return View();
        }
        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file) {
            if (file != null) {
                if (file.ContentLength > 0) {
                    string filename = Path.GetFileName(file.FileName);
                    string path = Path.Combine(Server.MapPath("~/Files/"), filename);
                    file.SaveAs(path);
                    string message = "Name：" + filename + "<br/>" + 
                                    "Content Type：" + file.ContentType + "<br/>" +
                                    "Size：" + file.ContentLength + "<br/>上傳成功";
                    TempData["Message"] = message;
                } else {
                    TempData["Message"] = "上傳空白檔案做啥?";
                }
            } else {
                TempData["Message"] = "忘記選取檔案了喔";
            }
            return RedirectToAction("Upload");
        }


        public ActionResult Content1() {
            return Content("Hello World");
        }
        public ActionResult Content2() {
            return Content("<root><data>Cowell MVC5</data></root>", "text/xml");
        }
        public ActionResult Content3() {
            return Content("<p>こんにちは</p>", "text/html", System.Text.Encoding.UTF8);
        }

        public ActionResult ShowTime() {
            return View();
        }
        public ActionResult getTime() {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("var t='{0}';\r\n", DateTime.UtcNow);
            return JavaScript(sb.ToString());
        }
    }
}