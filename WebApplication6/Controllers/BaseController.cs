﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Helpers;

namespace WebApplication6.Controllers
{
    public abstract class BaseController : Controller
    {
        public string Culture { get; set; }

        protected override void ExecuteCore() {
            string cultureName = null;
            HttpCookie cc = Request.Cookies["_culture"];
            if (cc != null)
                cultureName = cc.Value;
            else
                cultureName = Request.UserLanguages[0];

            cultureName = CultureHelper.GetImplementedCulture(cultureName);

            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            base.ExecuteCore();
        }
        protected override bool DisableAsyncSupport {
            get { return true; }
        }
#if DEBUG
        public ActionResult debug() {
            return View();
        }
#endif
    }

}