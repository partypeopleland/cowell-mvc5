﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace WebApplication6.Helpers {
    public class CultureHelper {
        private static readonly IList<string> _cultures = new List<string>{
            "zh-TW",
            "zh-CN",
            "en-US"
        };

        public static string GetDefaultCulture() {
            return _cultures[0];
        }

        public static string GetImplementedCulture(string name) {
            //檢查欲查詢的語系是否為空字串，若為空字串則傳回預設語系
            if (string.IsNullOrEmpty(name))
                return GetDefaultCulture();

            //如果欲查詢的語系是屬於已經實作的語系之一，則回傳該已實作的語系
            if (_cultures.Where(c => c.Equals(name, StringComparison.InvariantCultureIgnoreCase)).Count() > 0) {
                return name;
            }

            //取得最接近的語系名稱
            var n = GetNetureCulture(name);
            foreach (var c in _cultures) {
                if (c.StartsWith(n))
                    return c;
            }

            //如果沒有合適的就回傳預設語系
            return GetDefaultCulture();
        }

        /// <summary>
        /// 取得目前語系名稱
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentCulture() {
            return Thread.CurrentThread.CurrentCulture.Name;
        }

        /// <summary>
        /// 取得目前語系的相近語系名稱
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentNetureCulture() {
            return GetNetureCulture(GetCurrentCulture());
        }


        /// <summary>
        /// 取得相近語系名稱
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetNetureCulture(string name){
            if (name.Length < 2)
                return name;
            return name.Substring(0, 2);
        }

        public static void SetCulture(string name) {
            //修正目前執行緒的語系
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(name);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
        }
    }
}