﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication6.App_Start {
    public class ViewEngineConfig {
        public static void RegisterViewEngines(ViewEngineCollection viewEngines) {
            // 清除所有 View Engine
            viewEngines.Clear();
            // 重新註冊 RazorViewEngine，如果你使用的是 WebForm ViewEngine 則是加入 WebFormViewEngine
            viewEngines.Add(new RazorViewEngine());
        }
    }
}