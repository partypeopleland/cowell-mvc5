using System;
using System.Linq;
using System.Collections.Generic;
	
namespace WebApplication6.Models
{   
	public  class RegionRepository : EFRepository<Region>, IRegionRepository
	{

	}

	public  interface IRegionRepository : IRepository<Region>
	{

	}
}