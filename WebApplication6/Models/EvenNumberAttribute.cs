﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication6.Models {
    public class EvenNumberAttribute : DataTypeAttribute {
        public EvenNumberAttribute():base("EvenNumber") {

        }
        public override bool IsValid(object value) {
            if (value == null) { return true; }
            int num;
            if (int.TryParse(value.ToString(), out num)) {
                if (num % 2 == 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }
}