namespace WebApplication6.Models
{
	public static class RepositoryHelper
	{
		public static IUnitOfWork GetUnitOfWork()
		{
			return new EFUnitOfWork();
		}		
		
		public static CategoriesRepository GetCategoriesRepository()
		{
			var repository = new CategoriesRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static CategoriesRepository GetCategoriesRepository(IUnitOfWork unitOfWork)
		{
			var repository = new CategoriesRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static CustomerDemographicsRepository GetCustomerDemographicsRepository()
		{
			var repository = new CustomerDemographicsRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static CustomerDemographicsRepository GetCustomerDemographicsRepository(IUnitOfWork unitOfWork)
		{
			var repository = new CustomerDemographicsRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static CustomersRepository GetCustomersRepository()
		{
			var repository = new CustomersRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static CustomersRepository GetCustomersRepository(IUnitOfWork unitOfWork)
		{
			var repository = new CustomersRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static EmployeesRepository GetEmployeesRepository()
		{
			var repository = new EmployeesRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static EmployeesRepository GetEmployeesRepository(IUnitOfWork unitOfWork)
		{
			var repository = new EmployeesRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static Order_DetailsRepository GetOrder_DetailsRepository()
		{
			var repository = new Order_DetailsRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static Order_DetailsRepository GetOrder_DetailsRepository(IUnitOfWork unitOfWork)
		{
			var repository = new Order_DetailsRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static OrdersRepository GetOrdersRepository()
		{
			var repository = new OrdersRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static OrdersRepository GetOrdersRepository(IUnitOfWork unitOfWork)
		{
			var repository = new OrdersRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static ProductsRepository GetProductsRepository()
		{
			var repository = new ProductsRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static ProductsRepository GetProductsRepository(IUnitOfWork unitOfWork)
		{
			var repository = new ProductsRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static RegionRepository GetRegionRepository()
		{
			var repository = new RegionRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static RegionRepository GetRegionRepository(IUnitOfWork unitOfWork)
		{
			var repository = new RegionRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static ShippersRepository GetShippersRepository()
		{
			var repository = new ShippersRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static ShippersRepository GetShippersRepository(IUnitOfWork unitOfWork)
		{
			var repository = new ShippersRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static SuppliersRepository GetSuppliersRepository()
		{
			var repository = new SuppliersRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static SuppliersRepository GetSuppliersRepository(IUnitOfWork unitOfWork)
		{
			var repository = new SuppliersRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static sysdiagramsRepository GetsysdiagramsRepository()
		{
			var repository = new sysdiagramsRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static sysdiagramsRepository GetsysdiagramsRepository(IUnitOfWork unitOfWork)
		{
			var repository = new sysdiagramsRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static TerritoriesRepository GetTerritoriesRepository()
		{
			var repository = new TerritoriesRepository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static TerritoriesRepository GetTerritoriesRepository(IUnitOfWork unitOfWork)
		{
			var repository = new TerritoriesRepository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		
	}
}