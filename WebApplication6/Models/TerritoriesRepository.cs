using System;
using System.Linq;
using System.Collections.Generic;
	
namespace WebApplication6.Models
{   
	public  class TerritoriesRepository : EFRepository<Territories>, ITerritoriesRepository
	{

	}

	public  interface ITerritoriesRepository : IRepository<Territories>
	{

	}
}