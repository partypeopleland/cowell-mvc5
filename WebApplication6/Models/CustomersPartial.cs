﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication6.Models {

    [MetadataType(typeof(CustomersMetaData))]
    public partial class Customers {

    }

    public partial class CustomersMetaData {
        [Required]
        [StringLength(10,MinimumLength=5)]
        public string CustomerID { get; set; }
        [Required]
        [StringLength(10, MinimumLength = 5)]
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
        public string ContactTitle { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        [EvenNumber]
        [Required]
        [RegularExpression(@"^(\d)+")]
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
    }
}