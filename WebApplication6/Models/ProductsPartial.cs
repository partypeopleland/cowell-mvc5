﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication6.Models {
    [MetadataType(typeof(ProductsMetaData))]
    public partial class Products : IValidatableObject {
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext) {
            //模型驗證規則
            if (this.UnitPrice < 20) {
                yield return new ValidationResult("最便宜的純喫茶都要20塊錢了，你是不是打錯價格了?", new string[] { "UnitPrice" });
            }
        }
        
    }
    public partial class ProductsMetaData {
        public int ProductID { get; set; }
        [StringLength(20)]
        [Required(ErrorMessageResourceType=typeof(Resources.Product), ErrorMessageResourceName="ProductNameRequired")]
        public string ProductName { get; set; }
        public Nullable<int> SupplierID { get; set; }
        public Nullable<int> CategoryID { get; set; }
        public string QuantityPerUnit { get; set; }
        [Required]
        [RegularExpression(@"^(\d)+")]
        public Nullable<decimal> UnitPrice { get; set; }
        public Nullable<short> UnitsInStock { get; set; }
        public Nullable<short> UnitsOnOrder { get; set; }
        public Nullable<short> ReorderLevel { get; set; }
        public bool Discontinued { get; set; }
    }
}